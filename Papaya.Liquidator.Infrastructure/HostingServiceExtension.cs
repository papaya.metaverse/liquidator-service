﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Papaya.Liquidator.Infrastructure.Data;

namespace Papaya.Liquidator.Infrastructure;

public static class HostingServiceExtension
{
    public static void AddDbContext(this IServiceCollection services, string connectionString) =>
        services.AddDbContext<AppDbContext>(options =>
            options.UseNpgsql(connectionString, x => x.MigrationsHistoryTable("__MigrationsHistory", "papaya")));
}