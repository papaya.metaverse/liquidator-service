﻿using Microsoft.EntityFrameworkCore;
using Papaya.Liqudator.Core.UserBalanceAggregate;

namespace Papaya.Liquidator.Infrastructure.Data;

public class AppDbContext : DbContext
{
    public DbSet<UserBalance> Balances { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
}