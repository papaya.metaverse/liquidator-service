﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Papaya.Liqudator.Core.Services;

public class LiquidatorService : ILiquidatorService, IHostedService
{
    private readonly ILogger<LiquidatorService> _logger;
    private readonly ISampleDependency _sampleDependency;

    public LiquidatorService(
        ILogger<LiquidatorService> logger,
        ISampleDependency sampleDependency)
    {
        _logger = logger;
        _sampleDependency = sampleDependency;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Application starting...");
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Application stopping...");
        return Task.CompletedTask;
    }
}