﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Papaya.Liqudator.Core.UserBalanceAggregate;

[Table("user_balance", Schema = "papaya")]
public class UserBalance 
{
    public long Id { get; set; }
    public string Wallet { get; set; }
    public decimal Rate { get; set; }
    public decimal Balance { get; set; }
    public DateTime SnapshotDate { get; set; }
}