﻿// See https://aka.ms/new-console-template for more information

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Papaya.Liqudator.Core.Services;
using Papaya.Liquidator.Infrastructure;
using Serilog;

var configuration = new ConfigurationBuilder()
    .AddEnvironmentVariables()
    .AddCommandLine(args)
    .AddJsonFile("appsettings.json")
    .Build();

var hostBuilder = Host.CreateDefaultBuilder(args)
.ConfigureHostConfiguration(c =>
{
    c.AddEnvironmentVariables()
        .AddCommandLine(args)
        .AddJsonFile("appsettings.json");
})
.ConfigureServices(services =>
{
    services.AddHostedService<LiquidatorService>();
    services.AddScoped<ISampleDependency, SmapleDeepndecyService>();
    var connectionString = configuration.GetConnectionString("PostgresConnection") ?? throw new Exception("Missing db connection string");;
    services.AddDbContext(connectionString);
    
})
.UseSerilog((context, serilogConfiguration) =>
{
    serilogConfiguration.ReadFrom.Configuration(context.Configuration);
});

using IHost host = hostBuilder
    .Build();

host.Run();
    